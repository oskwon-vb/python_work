#-*- coding: utf-8 -*-
# ===============================================================
# Author:   coozplz@gmail.com
# File:     find_missing_number.py
# Date:     2015. 07. 30
# Desc:     한글 컨텐츠 중에 누락되는 파일을 찾기 위한 프로그램
# ===============================================================
 
import os

if __name__ == '__main__':
	
	path = "D:/git/flash_korean_novice/resources/ai/verb"	
	# r = range(1, 321) #명사
	r = range(321, 422) # 동사
	# r = range(471, 521) #형용사
	a_original = set(r)	
	b_original = set(r)	
	c_original = set(r)
	d_original = set(r)
	
	a_current = set()
	b_current = set()
	c_current = set()
	d_current = set()
	
	for filename in os.listdir(path):
		intValue = int(filename[0:3])
		code = filename[-5]		
		if (code == 'a'):
			a_current.add(intValue)
		elif (code == 'b'):
			b_current.add(intValue)
		elif (code == 'c'):
			c_current.add(intValue)
		elif (code == 'd'):
			d_current.add(intValue)		
	

	a_list = list(a_original - a_current)
	b_list = list(b_original - b_current)
	c_list = list(c_original - c_current)
	d_list = list(d_original - d_current)	
	
	a_list.sort()
	b_list.sort()
	c_list.sort()
	d_list.sort()
	
	print "A=%s" % a_list
	print "B=%s" % b_list
	print "C=%s" % c_list
	print "D=%s" % d_list
	
		
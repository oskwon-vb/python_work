__author__ = 'coozplz'
import xml.etree.ElementTree as etree
from os import listdir
from os.path import join
from file.FileUtils import FileUtils
from dao.kanji_dao import KanjiDAO
import sys
import re


def main():
    """
    main 함수
    :return:
    """
    root_file_path = "D:\\temp\\xml"
    kanji_dao = KanjiDAO("C:\\Users\\Ohsung\\Downloads\\kanji.sqlite")
    for dir_name in listdir(root_file_path):
        dir_absolute_path = join(root_file_path, str(dir_name))
        for f in listdir(join(root_file_path, str(dir_name))):
            xml_file_path = join(dir_absolute_path, str(f))
            #
            # load xml file
            #
            str_xml = FileUtils.read(xml_file_path)

            try:
                tree = etree.fromstring(str_xml)
                for child in tree.findall('category'):
                    for kanji_element in child.findall('kanzi'):
                        name = kanji_element.get('name')
                        anime = kanji_element.get('anime')
                        code = kanji_dao.select_kanzi_code_by_kanzi(name)
                        if not bool(re.match('\d', str(code))):
                            print("can't find kanji code %s" % (str(code)))
                            continue

                        kanji_element.set('anime', str(code))

                bytes_xml = etree.tostring(tree, 'utf-8', method='xml')
                str_xml = bytes_xml.decode('utf-8')
                FileUtils.write(xml_file_path, str_xml)
                # print(str_xml)

                # print(str(f).split('.')[0])
            except(RuntimeError, TypeError, NameError):
                 print("Unexpected error:", sys.exc_info()[0])
                 raise

main()
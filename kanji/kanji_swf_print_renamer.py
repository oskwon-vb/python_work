#!/usr/bin/python
"""
# Data: 2014. 09. 03
# Author: coozplz
# Desc: 파일명이 한자인 파일명을 SQLite Kanzi_mst 테이블에 있는 Kanzi Code 값으로 변경한다.
    1. 특정 폴더의 이미지 파일을 모두 조회한다.
        1.1 "한자.swf" 포맷을 가지는 파일을 모두 조회한다.
        1.2 "한자-a.jpg", "한자-b.jpg", "한자-c.jpg", "한자-d.jpg" 파일을 모두 조회한다.
    2. SQLite Kanzi_mst 테이블에서 Kanzi_code 를 조회한다.
        SELECT kanzi_code FROM kanzi_mst WHERE kanzi = '검색한자'
"""
import os
from dao.kanji_dao import KanjiDAO
import argparse

# parser = argparse.ArgumentParser()
# parser.add_argument('swf_dir', help='Directory absolute path of swf files', type=str, default='D:/temp/swf')
# parser.add_argument('print_dir', help='Directory absolute path of print images', type=str, default='D:/temp/print')
# parser.add_argument('db_path', help='File absoulute path of SQLite File', type=str, default='D:/temp/kanji.sqlite')
# args = parser.parse_args()
# db_path = args.db_path
# swf_dir = args.swf_dir
# print_dir = args.print_dir


def rename_swf(kanzi_dao, root_path):
    """
    swf 파일을 변경하는 로직
    :param root_path: swf 파일 저장 경로
    """
    for path, subdirs, files in os.walk(root_path):
        for name in files:
            kanzi = name[0]
            code = kanzi_dao.select_kanzi_code_by_kanzi(kanzi)
            if code is None:
                continue
            new_name = os.path.join(path, name.replace(kanzi, str(code)))
            old_name = os.path.join(path, name)
            os.rename(old_name, new_name)
    print('Done rename swf!!')


def rename_print_image(kanzi_dao, root_path):
    """
    4개의 Print 이미지를 변경한다.
    :param kanzi_dao: SQLite 조회 클래스
    :param root_path:  Print이미지 저장 경로
    """
    for path, subdirs, files in os.walk(root_path):
        for name in files:
            kanzi = name[0]
            code = kanzi_dao.select_kanzi_code_by_kanzi(kanzi)
            if code is None:
                continue
            new_name = os.path.join(path, name.replace(kanzi, str(code)))
            old_name = os.path.join(path, name)
            os.rename(old_name, new_name)
    print('Done rename print image!!')


def main():
    """
    main 함수
    :return:
    """
    db_path = 'D:/temp/kanji.sqlite'
    swf_dir = "D:/temp/swf"
    print_dir = "D:/temp/print"

    if not os.path.exists(db_path):
        raise FileNotFoundError("can't find db file")
    if not os.path.isfile(db_path):
        raise TypeError("db_path is not file")

    if not os.path.exists(swf_dir):
        raise TypeError("Swf files path does not exist !!")
    if not os.path.exists(swf_dir):
        raise TypeError("Print image path Does not exist !!")

    kanzi_dao = KanjiDAO(db_path)
    rename_swf(kanzi_dao, swf_dir)
    rename_print_image(kanzi_dao, print_dir)


main()

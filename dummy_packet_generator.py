import socket
import sys
import os

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error as msg:
    print('Failed to create socket. Error code: ' + str(
        msg[0]) + ', error message: ' + msg[1])
    sys.exit()
print('Socket is created')

host = '192.168.10.123'
port = 1843

try:
    remote_ip = socket.gethostbyname(host)

except socket.gaierror:
    print('hostname could not be resolved.')
    sys.exit()

s.connect((remote_ip, port))
print('socket is connected')

data = open('resources/test.jpg', 'wb+')
print(data.tell())
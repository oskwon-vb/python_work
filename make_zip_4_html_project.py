#!/usr/bin/python3
#-*-coding: UTF-8 -*-
"""
고객에게 전달하기 위한 압축 파일을 생성하는 모듈
상위 경로에서 모듈 이름을 입력하면 해당 yyyyMMdd_MUDULENAME.zip 형태의 압축 파일이 만들어진다.
"""
import zipfile
import os
import datetime
import argparse

#Constants
DEBUG = False 

#입력 파라미터 
parser = argparse.ArgumentParser()
parser.add_argument('module_name', help='Directory name for archive', type=str)
args = parser.parse_args()
MODULE_NAME = args.module_name

#압축시 제외시킬 항목
IGNORE_NAME = ['.git', '.gitignore', '.idea', '0_Document', 'README.md']


class ZipUtilities:
    """
    #압축을 실행하는 클래스
    """
    def to_zip(self, file, filename):
        """
        # zip 파일을 생성한다.
        """
        zip_file = zipfile.ZipFile(filename, 'w')
        if os.path.isfile(file):
            zip_file.write(file)
        else:
            self.add_folder_to_zip(zip_file, file)
        zip_file.close()

    def add_folder_to_zip(self, zip_file, folder):
        """
        # 하위 폴더를 포함한 모든 폴더를 재귀적으로 탐색하며 압축을 진행한다.
        # 만약 IGNORE_NAME 에 포함된 파일인 경우 압축 대상에 추가하지 않는다.
        """
        for file in os.listdir(folder):
            if DEBUG:
                print(file in IGNORE_NAME)
            if file in IGNORE_NAME:
                print(file)
                continue
            full_path = os.path.join(folder, file)
            if os.path.isfile(full_path):
                if DEBUG:
                    print('File added: ', str(full_path))
                zip_file.write(full_path)
            elif os.path.isdir(full_path):
                if DEBUG:
                    print('Entering folder: ', str(full_path))
                self.add_folder_to_zip(zip_file, full_path)


def main():
    """
    메인 함수
    """
    now = datetime.datetime.now()
    # 파일명을 생성 부분 "YYYYMMDD_{MODULE_NAME}.zip" 으로 생헌다.
    archive_file_name = now.strftime('%Y%m%d') + '_' + MODULE_NAME + '.zip'
    utilities = ZipUtilities()
    utilities.to_zip(MODULE_NAME, archive_file_name)

# 함수 실행부
main()

#!/usr/bin/python3
"""
# 폴더에 있는 파일중 파일 이름의 길이가 입력된 제한 값 보다 
# 긴 파일명을 가지는 파일을 삭제한다.
"""
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("length", help="length of file name for delete", type=int)
args = parser.parse_args()

# 사용자가 입력한 길이를 가져온다.
length = args.length

# 현재 경로를 가져온다.
current_path = os.getcwd()
print(current_path)

dirs = os.listdir(current_path)
# 결과 출력을 위한 변수
count = 0

for file in dirs:
    fileNameLength = len(file);
    if fileNameLength > length:
        os.remove(file)
        count += 1
print(count, " files are removed")
